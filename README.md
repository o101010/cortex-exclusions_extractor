# Cortex - exclusions extractor

Collect the list of the alert exclusions enabled on a specific tenant.

It's actually designed for Edge on Windows x64, but feel free to adapt the
 code for your environment.

## Remarques

This script has been written for my personal needs, with quick and fast development
 in mind. You must rewrite some parts of the code to match your needs.

For example:

- I filter the disabled alert exclusions directly during the request to the Cortex
  API (*[main.py](main.py) > function collectXdrAlertExclusions > variable json_data*)

- I extract and format the description of the alert exclusions with regex
  (*[main.py](main.py) > function formatXdrAlertExclusionComment*)

Consider using your browser DevTools or a proxy to find your query settings.

## Installation

### Python on the system

If you already have a Python execution environment on the system or plan to
 install one, download the repo and simply run the following commands :

```shell
python -m venv venv
./venv/script/activate
pip install -r requirements.txt
python main.py
```

### Python Portable

If you don't want to install Python on your device, download the repo and
 run the following commands:

```shell
setSet-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process
./pythonPortable.py
```

It will download a portable python embed, create a dedicated python virtualenv
 and install the requirements for you.

## Usage

Run `run.bat`, log to your Palo Alto account and choose a tenant. Now, let the
 magic happen.

You can also play `main.py` with Python from a terminal. Don't forget to activate
 your virtualenv in this case.

## Improvements

- [ ] Uses the browser's user profile

- [ ] Check if selenium's WebDriver is present and download it if necessary

- [ ] Alert the user to choose a tenant if it's not done after x seconds on the
 tenant account selector gateway

## Credits

[Python](https://www.python.org/)

[Selenium](https://www.selenium.dev/)

[Toptal's gitignore.io](https://www.toptal.com/developers/gitignore)

[Palo Alto Cortex XDR](https://www.paloaltonetworks.com/cortex/cortex-xdr)

## Disclaimers

- I'm not affiliated with Palo Alto of any kind. All copyrights about their
 products belong to them.
- This script don't use official API but request "like a browser". The url or any
 other component may be broken after Palo Alto modifications
