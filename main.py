#!/usr/bin/env python

__author__ = "o101010"
__copyright__ = "Copyright 2024, o101010"
__credits__ = ["o101010"]

__license__ = "GNU GPL version 3"
__version__ = "0.0.1"
__maintainer__ = "o101010"
__email__ = "o101010@gitlab"
__status__ = "development"


# from os import path
from sys import exit as sys_exist
from time import time_ns
from re import search as regex_search
from re import findall as regex_find_all
from json import loads as json_load
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from requests import post


DEFAULT_BROWSER_USERPROFILE = "%LOCALAPPDATA%\\Microsoft\\Edge\\User Data"
XDR_URL_REGEX = r"[a-z\-]+.xdr.[a-z]{2}.paloaltonetworks.com"


def checkSelenium():
    return True  # TODO


def installSelenium():
    return True  # TODO


def collectXdrCookiesAndTenant():
    # options = webdriver.EdgeOptions() # TODO
    # profile_path = path.expandvars(DEFAULT_BROWSER_USERPROFILE)
    # options.add_argument("user-data-dir={profile_path}")
    # options.add_argument("profile-directory=Default")
    # service = webdriver.EdgeService(executable_path="msedgedriver.exe")
    # driver = webdriver.Edge(service=service, options=options)

    cookies = []
    tenant = ""

    options = webdriver.EdgeOptions()
    options.set_capability('unhandledPromptBehavior', 'ignore')
    driver = webdriver.Edge(options=options)

    try:
        driver.get("https://cortex-gateway.paloaltonetworks.com/")
        # Waiting the use enter the credentials and select a tenant
        while (driver.current_url != "https://cortex-gateway.paloaltonetworks.com/accounts"):
            # Waiting for log in
            pass

        # waiting the user choose a tenant
        pos = 0
        start_time = time_ns()
        trigger_alert = False
        while regex_search(XDR_URL_REGEX, driver.current_url) is None:
            if not trigger_alert:
                if time_ns() - start_time > 30000000000:  # 30s
                    print("alert(\"Please, choose a tenant to continue.\")")  # TODO
                    trigger_alert = True
            else:
                if time_ns() - start_time > 3600000000000:  # 1h
                    driver.quit()
                    sys_exist("You take too long to choose your tenant."
                              " I prefer to kill myself to free your computer ressources.")
            pos += 1
            if pos >= len(driver.window_handles):
                pos = 0
            driver.switch_to.window(driver.window_handles[pos])

        wait = WebDriverWait(driver, 40)
        wait.until(EC.url_contains("dashboard"))

        cookies = driver.get_cookies()
        tenant = driver.current_url.split("/")[2]  # ["https:", "", "domaine", "folders", ...]
    except TimeoutException:
        driver.quit()
        sys_exist("XDR Dashboard took too long to load. I'm sure it's crashed in a wall."
                  " I preferred to leave.")
    finally:
        driver.quit()
    return cookies, tenant


def collectXdrAlertExclusions(cookies: dict, domain: str):
    domain = regex_search(XDR_URL_REGEX, domain).group()
    url = f"https://{domain}/api/webapp/get_data"
    # Filter on exclusion "status equal enable" and sort by exclusion ID
    params = {'type': 'grid', 'table_name': 'ALERTS_WHITELISTS_TABLE', 'data_id': 'null'}
    headers = {}
    headers["Content-Type"] = "application/json"
    headers["pan-lang-cookie"] = "en_US"
    headers["x-xsrf-token"] = cookies["XSRF-TOKEN"]
    headers["x-csrf-token"] = cookies["csrf_token"]
    json_data = '{"extraData": null, "filter_data": { "sort": [{ "FIELD": "ALERT_WHITELIST_ID", "ORDER": "ASC" }],' \
        ' "filter": { "AND": [{ "SEARCH_FIELD": "ALERT_WHITELIST_STATUS",' \
        ' "SEARCH_TYPE": "EQ", "SEARCH_VALUE": "ENABLED" }]},' \
        ' "free_text": "", "visible_columns": null, "locked": null, "paging": { "from": 0, "to": 100 } }, "jsons": []}'
    ret = post(url, headers=headers, data=json_data, cookies=cookies, params=params)

    return ret.status_code, ret.content


def formatXdrAlertExclusionComment(comment: str):
    extracted = regex_find_all(r'(?:#)?(\d{5,})', comment)
    newComment = ""
    if extracted is not None:
        elements = []
        for element in extracted:
            if element[0] != "#":
                element = "#" + element
            elements.append(element)
            newComment = " - ".join(elements)
    return newComment


def parseAndPrintXdrAlertExclusions(alertExclusions: json_load):
    alertExclusions = alertExclusions['reply']
    nb_disable = int(alertExclusions['TOTAL_COUNT']) \
        - int(alertExclusions['FILTER_COUNT'])
    print("\n\n{}\n{} alert exclusions have been found (plus {} disabled).\n"
          .format("-"*30, alertExclusions['FILTER_COUNT'], nb_disable))

    alertExclusions = alertExclusions['DATA']
    for exclusion in alertExclusions:
        description = formatXdrAlertExclusionComment(
            exclusion["ALERT_WHITELIST_COMMENT"])
        if description:
            output = f"{description} : {exclusion["ALERT_WHITELIST_NAME"]}"
        else:
            output = exclusion["ALERT_WHITELIST_NAME"]
        print(output)


def main():
    cookies_list, tenant_url = collectXdrCookiesAndTenant()
    if not cookies_list:
        sys_exist("ERROR : No cookies... I'm angry. :'(")
    if not tenant_url:
        sys_exist("ERROR : Tenant url not collected ?!")
    cookies = {}
    for cookie in cookies_list:
        cookies[cookie['name']] = cookie['value']

    httpCode, httpData = collectXdrAlertExclusions(cookies, tenant_url)
    if httpCode == 200:
        httpData = json_load(httpData)
    else:
        sys_exist(f"ERROR : http code {httpCode}: {httpData}")

    """with open(".tests\\exclusions.json", "r") as f:
        httpData = f.readline()
        httpData = json_load(httpData)"""

    parseAndPrintXdrAlertExclusions(httpData)


if __name__ == "__main__":
    main()
