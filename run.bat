@echo off


if exist venv (
    call venv/Scripts/activate.bat
) else if exist .venv (
    call .venv/Scripts/activate.bat
)
python main.py

deactivate
pause
