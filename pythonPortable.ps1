<#
.SYNOPSIS
    Create a python virtualenv based on a portable python bundle.
#>


$url = "https://www.python.org/ftp/python/"
$version = 3
$VENVNAME = "venv"
$PYVERSION = "3.12.3"  # TODO Python 3.13.0 is broken
# $DebugPreference = "Continue"


if ((Test-Path variable:VENVNAME) -and ![string]::IsNullOrEmpty($variable)) {
    $VENVNAME = "$PSScriptRoot\$VENVNAME"
} else {
    $VENVNAME = "$PSScriptRoot\venv"
}
if (Test-Path -Path $VENVNAME) {
    Write-Error "[ERROR] : File exist: $VENVNAME"
    Return -1
}


if ((Test-Path variable:PYVERSION) -and ![string]::IsNullOrEmpty($VENVNAME)) {
    $version = $PYVERSION
 } else {
    # Check last version of python
    $versions = (Invoke-WebRequest -Uri $url).Links.Href | Select-String -Pattern "^$version\.\d+\.\d+" | ForEach-Object {$_.Matches.Value}
    $versions = $versions | ConvertFrom-Csv -header 'Version', 'Major', 'Minor' -Delimiter '.'
    $major = ($versions | Measure-Object -Property 'Major' -Maximum).Maximum
    $minor = ($versions | Where-Object {$_.Version -eq $version -and $_.Major -eq $major} | Measure-Object -Property 'Minor' -Maximum).Maximum
    $version = "$version.$major.$minor"
}
Write-Debug "Python $version will be used"

# Check last release of the embed package
$url = "$url/$version/"
$package = (Invoke-WebRequest -Uri $url).Links.Href | Select-String -Pattern "^python-$version.*-embed-amd64.zip$"
$package = ($package | ConvertFrom-Csv -header 'Version' | Sort-Object -Property Version -Descending | Select-Object -First 1).Version
$url = "$url/$package"
Write-Debug "[DEBUG] Url : $url"

# Generate a temp dir
$tempDir = -join ((48..57) | Get-Random -Count 32 | ForEach-Object {[char]$_})
while (Test-Path "$env:TEMP\$tempDir") {
    $tempDir = -join ((48..57) | Get-Random -Count 32 | ForEach-Object {[char]$_})
}
$null = New-Item -Path $env:TEMP -Name $tempDir -ItemType Directory
$tempDir = "$env:TEMP\$tempDir"
Write-Debug "[DEBUG] Temp dir : $tempDir"

Write-Debug "[DEBUG] Download and extract python $version embed file"
Invoke-WebRequest -Uri $url -OutFile "$tempDir/$package"
Expand-Archive "$tempDir\$package" -DestinationPath "$tempDir/python"

Write-Debug "[DEBUG] Installation pip for the embed python"
# https://michlstechblog.info/blog/python-install-python-with-pip-on-windows-by-the-embeddable-zip-file/
Invoke-WebRequest -uri https://bootstrap.pypa.io/get-pip.py -OutFile "$tempDir/python/get-pip.py"
$pthFile = (Get-ChildItem -Path "$tempDir\python\python*._pth").Name
$pthFile = "$tempDir\python\$pthFile"
Add-Content -Path $pthFile -Value "$tempDir\python"
Add-Content -Path $pthFile -Value "$tempDir\python\DLLs"
Add-Content -Path $pthFile -Value "$tempDir\python\lib"
Add-Content -Path $pthFile -Value "$tempDir\python\lib\plat-win"
Add-Content -Path $pthFile -Value "$tempDir\python\lib\site-packages"
& $tempDir\python\python.exe "$tempDir\python\get-pip.py" --no-warn-script-location
Write-Debug "[DEBUG] Installation virtualenv for the embed python"
& $tempDir\python\python.exe -m pip install virtualenv --no-warn-script-location
& $tempDir\python\python.exe -m virtualenv "$VENVNAME"
if (-not (Test-Path $VENVNAME)) {
    Write-Error "[ERROR] Venv \"$VENVNAME\" not created"
}

Start-Sleep -Seconds 5
Write-Debug "[DEBUG] Remove temporary files"
Remove-Item -Path $tempDir -Recurse -Force

# Install the requirements.txt in the venv
if (Test-Path requirements.txt) {
    Write-Debug "[DEBUG] install requirements.txt"
    & $VENVNAME/Scripts/activate.ps1
    & $VENVNAME/Scripts/python.exe -m pip install -r requirements.txt --no-warn-script-location
    deactivate
}

Write-Output "The new virtualenv is available: $VENVNAME"
